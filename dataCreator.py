import matplotlib.pylab as plt
from operator import itemgetter
import numpy as np

def extractElem(elemToExtract, data):
    return np.concatenate(list(map(itemgetter(elemToExtract), data)))


markerColor_dict = {0 : 'gold', 1 : 'blue', 2 : 'darkred', 3 : 'green', 4 : 'purple', 5: 'darkorange', 6: 'gray'}
bgColor_dict = {0 : 'yellow', 1 : 'cyan', 2 : 'tomato', 3 : 'lime', 4 : 'fuchsia', 5: 'orange', 6:'lightgray'}

def plotSpiral(spiralData):
    plt.figure()
    plt.scatter(*(extractElem(0, spiralData)).T, c=extractElem(1, spiralData), s=40)
    plt.show()

numbClasses = 12

numbInClass = 200

def makeSpiral(spId, numbClasses):
    radius = np.linspace(0.5, 20, numbInClass)
    origTheta = 2 * np.pi / float(numbClasses)
    theta = np.linspace(spId * origTheta, (spId+4) * origTheta, numbInClass)
    theta += 0.17 * np.random.uniform(low = -1, high = 1, size = numbInClass) * np.linspace(1, 1.5, numbInClass)
    xPos = radius * np.cos(theta) +20 #oofset!
    yPos = radius * np.sin(theta) +20
    return np.c_[xPos, yPos], np.array([spId] * numbInClass)


def switch_classes(class1, class2):
    indx_to_replace = np.random.choice(numbInClass, numbInClass * 4 // 10, replace=False)  # 40% error
    temp = spiralData[class1][indx_to_replace,:]
    spiralData[class1][indx_to_replace,:] = spiralData[class2][indx_to_replace,:]
    spiralData[class2][indx_to_replace, :] = temp


spiralData = list([makeSpiral(x, numbClasses) for x in range(numbClasses)])
plotSpiral(spiralData)

spiralData = np.array([spiralData[x][0] for x in range(numbClasses)])
print(spiralData[8][:,:])
switch_classes(8,11)
print('and now')
print(spiralData[8][:,:])

np.save('data.npy',spiralData)
